# mario

Print out a pyramid that looks like the one at the end of every level in SMB

Example:

    $ ./mario
    Height: 5
        ##
       ###
      ####
     #####
    ######